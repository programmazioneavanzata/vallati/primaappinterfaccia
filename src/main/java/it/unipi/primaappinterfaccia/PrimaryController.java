package it.unipi.primaappinterfaccia;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PrimaryController {
    
    @FXML private TextField usr;
    @FXML private PasswordField pwd;
    
    private static final Logger logger = LogManager.getLogger(PrimaryController.class);
    
    @FXML
    private void switchToSecondary() throws IOException {
        
        logger.debug("Login premuto!");
        
        if(usr.getText().equals("carlo") && pwd.getText().equals("segreto")){
            logger.debug("Password corretta!");
            App.setRoot("secondary");
        }
    }
}
