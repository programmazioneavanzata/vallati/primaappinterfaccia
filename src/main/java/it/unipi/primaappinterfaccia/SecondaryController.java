package it.unipi.primaappinterfaccia;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SecondaryController {

    private static final Logger logger = LogManager.getLogger(SecondaryController.class);
    
    @FXML private ImageView imgCat;
    
    @FXML private VBox stackSec;
    
    @FXML
    public void initialize() {
        logger.debug("Seconda schermata mostrata!");
        
    }
    
    @FXML
    private void bottonePremuto() throws IOException {
        logger.debug("Cambio in cane premuto!");
        imgCat.setImage(new Image(getClass().getResource("img/dog.png").toExternalForm()));
        
        Button b = new Button();
        b.setText("Nuovo bottone");
        
        // action event
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
                b.setText("Bottone premuto");
                imgCat.setVisible(false);
            }
        };
        
        b.setOnAction(event);
        stackSec.getChildren().add(b);
        
        //App.setRoot("primary");
    }
    
}