module it.unipi.primaappinterfaccia {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.logging.log4j;

    opens it.unipi.primaappinterfaccia to javafx.fxml;
    exports it.unipi.primaappinterfaccia;
}
